<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	
	<?php if(get_field('header_image', 'option')) { ?>
	<style>
		.banner-overlay:before {
			content: ' ';
			display: block;
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 190px;
			z-index: -1;
			background-image: url(<?php the_field('header_image', 'option'); ?>);
			background-repeat: no-repeat;
			background-position: 50% 0;
			background-size: cover;
			background-blend-mode: multiply;
			background-color: rgba(27, 72, 137, 1);
		}
		header.top {
			background-color: rgba(27, 72, 137, .75);
		}
	</style>
	<?php } ?>
	
	<header role="banner" class="top">
		<div class="banner-overlay">
			<div class="content">
				<a href="<?php echo home_url(); ?>" class="university-logo">
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo-white.svg" alt="UCLA" />
				</a>
				<div class="dept-logo">             
				    <?php // Custom Logo code start
					if (has_custom_logo()) {  // Display the Custom Logo
						the_custom_logo();
					} else {  // No Custom Logo, just display the site's name ?>
					<a href="<?php echo home_url(); ?>"><h1 class="logo-text" alt="<?php the_field('department_name', 'option'); ?>"><?php bloginfo('name'); ?></h1></a>
					<?php } // Custom Logo code ends ?>
				</div>
				<?php if(get_field('enable_donation', 'option') == "enable") { ?>
				<div class="give-back">
					<?php if(get_field('link_type', 'option') == "internal") { ?>
					<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
					<?php }?>
					<?php if(get_field('link_type', 'option') == "external") { ?>
					<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
					<?php }?>
					<?php the_field('button_text', 'option'); ?></a>
					<?php if(get_field('supporting_text', 'option')) { ?>
					<span class="support"><?php the_field('supporting_text', 'option'); ?></span>
					<?php }?>
				</div>
				<?php }?>
			
				<nav role="navigation" aria-label="Main Navigation" class="desktop">
					<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Main Menu', 'bonestheme' ),
						'menu_class' => 'main-nav',
						'theme_location' => 'main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
					)); ?>
				</nav>
				<?php get_search_form(); ?>
			</div>
		</div>
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( ! is_front_page() ) { 
			
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
				<div id="hero" style="background-image: url('<?=$url?>');">
					<div class="content">
					<?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
					    <div class="hero-container">
					        <div class="hero-description">
					            <p class="caption"><?php echo $caption; ?></p>
					        </div>
					    </div>
					<?php endif; ?>
					</div> 
					&nbsp;
				</div>
			<?php }
			// If a category page
			if ( is_archive() ) {
				$thumb = apply_filters( 'taxonomy-images-queried-term-image-url', '', array(
					'image_size' => 'hero'
				) );
				if(!empty($thumb)){
				?>
					<div id="hero" style="background-image: url('<?=$thumb?>');">
						&nbsp;
					</div>
				<?php }
			}
			// If event landing page
			if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() ) {
				$image = get_field('event_header_image', 'option');
				if( $image ):

					// Image variables.
					$url = $image['url'];
					$title = $image['title'];
					$alt = $image['alt'];
					$caption = $image['caption'];

					// Thumbnail size attributes.
					$size = 'hero';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ];
					?>
					<div id="hero" style="background-image: url('<?php $thumb; ?>');">
						&nbsp;
					</div>
				<?php endif;
				}
			// Breadcrumbs
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		} ?>