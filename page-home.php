<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image'); // get all the rows
					$rand_row = $rows[ array_rand($rows) ]; // get a random row
					$silder_image = $rand_row['image']; // get the sub field value
					$slider_title = $rand_row['title']; // get the sub field value 
					$slider_description = $rand_row['description']; // get the sub field value 
					$slider_button = $rand_row['button_text']; // get the sub field value 
					$slider_link = $rand_row['link']; // get the sub field value 			
					if(!empty($silder_image)): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if($silder_image): ?>
				<?php if($slider_link): ?>
				<a href="<?php echo $slider_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
						<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
						<?php if($slider_title || $slider_description): ?>
							<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
								<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
									<?php if($slider_title): ?>
									<h2><?php echo $slider_title; ?></h2>
									<?php endif; ?>
									<?php if($slider_description): ?>
									<p><?php echo $slider_description; ?></p>
									<?php endif; ?>
									<?php if($slider_button): ?>
									<span class="btn outline"><?php echo $slider_button; ?></span>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if($slider_link): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
				if(get_field('hero_type', 'option') == "slider") { ?>
				<script type="text/javascript">
					jQuery("document").ready(function($) {
						$(document).ready(function(){
						  $('#bxslider').bxSlider({
						  	autoHover: true,
						  	auto: false,
						  });
						});
					});
				</script>
				<div id="slider">
					<ul id="bxslider">
						<?php if(have_rows('hero_image')): ?>
						<?php while(have_rows('hero_image')): the_row(); ?>
						<?php
							$slider_title = get_sub_field('title');
							$slider_description = get_sub_field('description');
							$slider_link = get_sub_field('link');
							$silder_image = get_sub_field('image');
							$slider_button = get_sub_field('button_text');
							$gradient = get_sub_field('show_gradient');
							if(!empty($silder_image)): 
								// vars
								$url = $silder_image['url'];
								$title = $silder_image['title'];
								// thumbnail
								$size = 'home-hero';
								$slide = $silder_image['sizes'][ $size ];
								$width = $silder_image['sizes'][ $size . '-width' ];
								$height = $silder_image['sizes'][ $size . '-height' ];
							endif;
						?>		
						<?php if($slider_link): ?>
						<a href="<?php echo $slider_link; ?>" class="hero-link">
						<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
							<?php if($gradient): ?>
								<div class="bg">
							<?php endif; ?>
									<div class="content">
										<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
											<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
												<?php if($slider_title): ?>
												<h2><?php echo $slider_title; ?></h2>
												<?php endif; ?>
												<?php if($slider_description): ?>
												<p><?php echo $slider_description; ?></p>
												<?php endif; ?>
												<?php if($slider_button): ?>
												<span class="btn outline"><?php echo $slider_button; ?></span>
												<?php endif; ?>
											</div>
										</div>
									</div>
							<?php if($gradient): ?>
								</div>
							<?php endif; ?>
							</li>
						<?php if($slider_link): ?>
						</a>
						<?php endif; ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<?php } ?>
				<section class="home-columns">
					<div class="content">
						<?php
						if(have_rows('homepage_columns')) :
							while (have_rows('homepage_columns')) : the_row();
								
								// For showing snippet from any page
								if(get_row_layout() == 'page_excerpt') 
									get_template_part('snippets/col', 'page');
						        
								// For showing list of recent post
								elseif(get_row_layout() == 'recent_posts') 
									get_template_part('snippets/col', 'posts');
								
								// For showing free form content
								elseif(get_row_layout() == 'content_block') 
									get_template_part('snippets/col', 'content');
								
								// For showing list of events from event widget
								elseif(get_row_layout() == 'upcoming_events') 
						       		get_template_part('snippets/col', 'events');
						       	
						       	// For showing a menu
						       	elseif(get_row_layout() == 'menu') 
						       		get_template_part('snippets/col', 'menu');
						       	
						       	// For showing a widget
					       		elseif( get_row_layout() == 'widget_1' ) 
					       			get_template_part('snippets/col', 'widget-1');
								
							endwhile;
						endif;
				    	?>
					</div>
				</section>
				<section class="publications">
					<div class="content">
						<?php if(get_field('publications_title')) { ?>
						<h3><?php the_field('publications_title'); ?></h3>
						<?php } ?>
						<?php if(get_field('publications_content')) { ?>
						<p><?php the_field('publications_content'); ?></p>
						<?php } ?>
						<script type="text/javascript">
							jQuery("document").ready(function($) {
								$(document).ready(function(){
									$('.books').bxSlider({
										slideWidth: 220,
										minSlides: 1,
										maxSlides: 4,
										slideMargin: 20,
										pager: false,
										moveSlides: 1,
									});
								});
							});
						</script>
						<?php
						$books = get_field('publications_books');
						if( $books ): ?>
						<ol class="books">
						    <?php foreach( $books as $post): ?>
						        <?php setup_postdata($post); ?>
						        <li>
						        	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="cover-link">
						        	<?php if(get_field('book_cover')) {
						        		$image = get_field('book_cover');
						        		if( !empty($image) ): 
						        			// vars
						        			$url = $image['url'];
						        			$title = $image['title'];
						        			// thumbnail
						        			$size = 'large-book';
						        			$thumb = $image['sizes'][ $size ];
						        			$width = $image['sizes'][ $size . '-width' ];
						        			$height = $image['sizes'][ $size . '-height' ];
						        		endif; ?>
						        		<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
						        		<?php } else { ?>
						        		<div class="custom-cover cover">
						        			<span class="title"><?php the_title(); ?></span>
						        		</div>
						        		<?php } ?>
						        	</a>
						        	<dl>
						        		<dt class="title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></dt>
						        		<?php if(get_field('publisher')) { ?>
						        		<dd class="publisher"><?php the_field('publisher'); ?>, <?php the_field('published_date'); ?></dd>
						        		<?php } ?>
						        		<dd class="author">
						        			<?php $author = get_field('author'); ?>
						        			<? if( $author ): ?>
						        			<?php foreach( $author as $post): ?>
						        			<?php setup_postdata($post); ?>
						        			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						        			<?php endforeach; ?>
						        			<?php wp_reset_postdata(); ?>
						        			<?php endif; ?>
						        		</dd>
						        	</dl>
						        </li>
						    <?php endforeach; ?>
						</ol>	
					    <?php wp_reset_postdata(); ?>
					<?php endif; ?>
						
					</div>
				</section>
			</div>
		</div>
<?php get_footer(); ?>