<?php
/*
 Template Name: Graduate Courses
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//$terms = get_field('quarter');
						//$quarter_list = array();
						//foreach( $terms as $term ) {
						//	$quarter_list[] = $term->slug;
						//}
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
					
					<?php if( have_rows('course_list') ): ?>
						<?php while( have_rows('course_list') ): the_row(); 
					
							// vars
							$quarter = get_sub_field('quarter');
							$quarter_slug = $quarter->slug;
							$quarter_name = $quarter->name;
							
							if( $quarter ): ?>
								<?php $quarter = new WP_Query( 
									array( 
										'post_type' => 'courses', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'quarter' => $quarter_slug	
									)
								); ?>
								<h2 id="courses"><?php echo $quarter_name; ?></h2>
								<ul class="course-list">
								<?php if ( $quarter->have_posts() ) : while ( $quarter->have_posts() ) : $quarter->the_post(); ?>
									<li>
										<h3><?php the_title(); ?></h3>
										<?php if(get_field('instructor_type') == "internal") { ?>
										<span class="instructors">
											<strong>Instructor: </strong>
											<?php $instructor = get_field('instructor'); ?>
											<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
											<?php if(get_field('additional_link')) { ?>
												<a href="<?php the_field('additional_link'); ?>">
											<?php } else { ?>
											<a href="<?php the_permalink() ?>">
											<?php } ?>
											<?php the_title(); ?></a>
											<?php $quarter->reset_postdata(); ?>
											<?php endif; ?>
										</span>
										<?php }	?>
										<?php if(get_field('instructor_type') == "external") { ?>
										<span class="instructors">
											<?php if(get_field('additional_instructors')) { ?>
											<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
											<?php } ?>
										</span>
										<?php }	?>
										<?php if(get_field('instructor_type') == "both") { ?>
										<span class="instructors">
											<strong>Instructor: </strong>
											<?php $instructor = get_field('instructor'); ?>
											<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
											<?php if(get_field('additional_link')) { ?>
												<a href="<?php the_field('additional_link'); ?>">
											<?php } else { ?>
											<a href="<?php the_permalink() ?>">
											<?php } ?>
											<?php the_title(); ?></a><?php $quarter->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
										</span>
										<?php }	?>
										<?php the_content(); ?>
									</li>
								<?php endwhile; else : ?>
								</ul>
								<p>There are no <?php the_field('program'); ?> courses this quarter.</p>
								<?php endif; ?>
								<?php wp_reset_postdata(); ?>
										
								<?php endif; ?>
					
						<?php endwhile; ?>
					
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>