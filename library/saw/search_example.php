<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include('search_function.php');

$subject_area_code = NULL; //array('ling', 'italian') or, NULL or empty array for all
$program = NULL; //string U or G, NULL or empty string for both
$quarters = array('18f'); //NULL or empty array for all
$do_not_include = array('165', '165B');//classes to remove from results (courseCatalogNumberDisplay) (DO NOT put leading zeros)

$list = list_courses_by_subject_area_code($subject_area_code, $program, $quarters, $do_not_include); 

foreach ($list as $subjectArea => $classes) {

    echo '<h1>'.$subjectArea.'</h1>';

    //CLASS INFO
    foreach ($classes as $class) {
        
        echo '<h2>'.$class["subjectClass"].'</h2>';

        echo '<b>Program/Career Level: </b>'. $class["careerLevel"] . '<br>';
        echo '<b>Description: </b>';

        //if course description is empty, then show class description
        echo (empty($class["courseDescription"]) ? $class["classDescription"] : $class["courseDescription"]);
        
        echo '<br>';

        //SECTION INFO
        echo '<h3>Instructors</h3>';
        if(empty($class["instructors"])){
            echo "No assigned instructors</br>";
            continue;
        }
        
        foreach($class["instructors"] as $instructor){
            echo $instructor['fullName'] . '</br>';
        }
    }
}

/*
echo "<pre>";
var_dump($list);
echo "</pre>";*/