				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) : 
						dynamic_sidebar( 'news-sidebar' ); 
					else : endif;
					
					if ( is_active_sidebar( 'events-sidebar' ) ) : 
						dynamic_sidebar( 'events-sidebar' ); 
					else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-label="Section Navigation">
							<?php
								wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Quick Links', 'bonestheme' ),
									'menu_class' => 'side-nav',
									'theme_location' => 'quick-links',
									'before' => '',
									'after' => '',
									'depth' => 2,
									'items_wrap' => '<h3>Quick Links</h3> <ul>%3$s</ul>'
								));
							?>
						</nav>
					</div>
				</div>
				<?php } ?>