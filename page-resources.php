<?php
/*
 Template Name: Resources Listing
*/
?>

<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<section>
						<?php the_content(); ?>
					</section>
					<ul class="program-list">
					<?php $resources_loop = new WP_Query( array( 'post_type' => 'resources', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1
					 ) ); ?>
					 <?php while ( $resources_loop->have_posts() ) : $resources_loop->the_post(); ?>
						<li class="program">
							<h3><a href="<?php the_field('resource_link'); ?>"><?php the_title(); ?></a></h3>
						<?php if(get_field('resource_type')) { ?>
							<span class="program-type"><?php the_field('resource_type'); ?></span>
						<?php } ?>
						<?php if(get_field('resource_email')) { ?>
							<span class="program-email"><strong>Email:</strong> <a href="mailto:<?php the_field('resource_email'); ?>"><?php the_field('resource_email'); ?></a>
							</span>
						<?php } ?>		
						<?php if(get_field('resource_phone_number')) { ?>
							<span class="program-phone"><strong>Phone:</strong> <?php the_field('resource_phone_number'); ?></span>
						<?php } ?>
							<?php the_content(); ?>
							<a href="<?php the_field('resource_link'); ?>" class="btn">Visit Website <span class="hidden"> for <?php echo $program_title; ?></span></a>
						</li>	
					<?php endwhile; ?>					
					</ul>
				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>